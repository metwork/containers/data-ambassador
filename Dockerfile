FROM python:3.8-slim as poetry

WORKDIR /app

ENV PIP_DEFAULT_TIMEOUT=100 \
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    PIP_NO_CACHE_DIR=1 \
    POETRY_VERSION=1.1.6

RUN pip install "poetry==$POETRY_VERSION"

COPY pyproject.toml poetry.lock ./


RUN poetry export -f requirements.txt --without-hashes > /root/requirements.txt

FROM python:3.8-slim as final

RUN mkdir /app
WORKDIR /app
COPY --from=poetry /root/requirements.txt /app

RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt 

COPY data_ambassador /app/data_ambassador
WORKDIR /app/data_ambassador
ENV PYTHONPATH=${PYTHONPATH}:/app/data_ambassador

CMD ["uvicorn", "--host", "0.0.0.0", "--port", "8888", "app:app", "--reload"]
