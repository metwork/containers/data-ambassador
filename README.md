# data-ambassador

Ambassador for accessing MetWork data

## Run

```bash
poetry run uvicorn --port 8888 data_ambassador:app --reload
```
