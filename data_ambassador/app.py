import os
import logging
from typing import Dict, Any, Optional
from pathlib import Path
from json.decoder import JSONDecodeError
import requests
from requests.exceptions import ConnectionError
from tenacity import retry, stop_after_attempt, wait_fixed  # type: ignore
from fastapi import FastAPI, Header, HTTPException
from fastapi.middleware.cors import CORSMiddleware

SPECTRUM_PATH = "/spectrum"
TEMP_DATA_PATH = "MWDA_TEMP_DATA_PATH"

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

logger = logging.getLogger("app")
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter("[%(asctime)s] [%(levelname)s] %(message)s"))
logger.addHandler(handler)


@app.get("/health")
async def health() -> Dict[str, Any]:
    return {"status": "ok"}


@app.post(f"{SPECTRUM_PATH}/{{data_id}}")
async def root(data_id: str, x_token: Optional[str] = Header(None)) -> Dict[str, Any]:
    logger.info("get spectrum data for data_id : %s", data_id)
    spectrum_path = Path(str(os.getenv(TEMP_DATA_PATH))) / f"{data_id}.mgf"
    if not spectrum_path.exists():
        logger.info("spectrum path %s not exists", spectrum_path)
        version, spectrum_id = data_id.split("-")
        data_host = os.getenv("DATA_HOST")
        url = f"{data_host}/fragmols/{spectrum_id}/get_mgf"
        logger.info("Fetch data from %s", url)
        headers = {
            "Authorization": f"Token {x_token}",
            "Host": "metwork.pharmacie.parisdescartes.fr",
        }

        response = get_data(url, headers)

        logger.info("Writing data to %s", spectrum_path)
        spectrum_path.write_text(response.json()["mgf"])
    return {
        "status": "ok",
    }


@retry(stop=stop_after_attempt(10), wait=wait_fixed(2), reraise=True)
def get_data(url: str, headers: Dict[str, str]) -> requests.Response:
    logger.info("get_data for %s", url)
    try:
        response = requests.get(url, headers=headers)
        status_code = response.status_code
        if status_code != 200:
            logger.error("Data host respond %s", status_code)
            try:
                detail = response.json()
            except JSONDecodeError:
                detail = response.text
            raise HTTPException(status_code=404, detail=detail)
        return response

    except ConnectionError as exc:
        logger.error("Connection Error %s", exc)
        raise HTTPException(status_code=404, detail="Connection Error")
