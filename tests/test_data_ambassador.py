import os
from pathlib import Path
from typing import Dict, Any
import pytest
from pytest import MonkeyPatch
from requests.exceptions import ConnectionError
from requests_mock.mocker import Mocker
from fastapi.testclient import TestClient
import data_ambassador
from data_ambassador.app import app, SPECTRUM_PATH, TEMP_DATA_PATH

DATA_HOST = "http://data_host"


@pytest.fixture(autouse=True)
def set_envs(monkeypatch: MonkeyPatch) -> None:
    monkeypatch.setenv("DATA_HOST", DATA_HOST)


def test_version() -> None:
    toml_path = Path(__file__).parents[1] / "pyproject.toml"
    version_line = toml_path.read_text().split("\n")[2]
    version = version_line.split(" = ")[1]
    assert version == f'"{data_ambassador.__version__}"'


@pytest.fixture
def client(shared_datadir: Path, monkeypatch: pytest.MonkeyPatch) -> TestClient:
    monkeypatch.setenv(TEMP_DATA_PATH, str(shared_datadir))
    return TestClient(app)


def test_health_check(client: TestClient, shared_datadir: Path) -> None:

    response = client.get("/health")

    assert response.status_code == 200


def test_get_spectrum_file_exists(client: TestClient, shared_datadir: Path) -> None:
    spectrum_id = "1"

    response = client.post(f"{SPECTRUM_PATH}/test-{spectrum_id}")

    assert response.status_code == 200


def test_get_spectrum_file_not_exists(
    client: TestClient, requests_mock: Mocker, shared_datadir: Path
) -> None:
    requests_mock.real_http = True  # type: ignore
    spectrum_id = "99"
    data_host = os.getenv("DATA_HOST")
    mocked_data = (shared_datadir / "test-1.mgf").read_text()
    url = f"{data_host}/fragmols/{spectrum_id}/get_mgf"
    requests_mock.get(url, json={"mgf": mocked_data})

    response = client.post(f"{SPECTRUM_PATH}/test-{spectrum_id}")

    assert response.status_code == 200


def test_get_spectrum_retry(
    client: TestClient,
    requests_mock: Mocker,
    shared_datadir: Path,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    requests_mock.real_http = True  # type: ignore
    spectrum_id = "99"
    data_host = os.getenv("DATA_HOST")

    def connection_error(*args: Any, **kwargs: Any) -> None:
        raise ConnectionError

    mocked_data = (shared_datadir / "test-1.mgf").read_text()
    url = f"{data_host}/fragmols/{spectrum_id}/get_mgf"
    requests_mock.get(
        url,
        [
            {"exc": ConnectionError},
            {"exc": ConnectionError},
            {"json": {"mgf": mocked_data}},
        ],
    )
    response = client.post(f"{SPECTRUM_PATH}/test-{spectrum_id}")

    assert response.status_code == 200


retries = 0


@pytest.mark.parametrize(
    "mock_kwargs",
    ({"text": '"error": "description"}'}, {"json": {"error": "description"}}),
)
def test_get_spectrum_data_host_error(
    client: TestClient,
    requests_mock: Mocker,
    shared_datadir: Path,
    mock_kwargs: Dict[str, Any],
) -> None:
    requests_mock.real_http = True  # type: ignore
    spectrum_id = "99"
    data_host = os.getenv("DATA_HOST")
    url = f"{data_host}/fragmols/{spectrum_id}/get_mgf"
    requests_mock.get(url, status_code=404, **mock_kwargs)

    response = client.post(f"{SPECTRUM_PATH}/test-{spectrum_id}")

    assert response.status_code == 404
